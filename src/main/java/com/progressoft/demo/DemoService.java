package com.progressoft.demo;

import com.progressoft.outbox.EventPublisher;
import lombok.AllArgsConstructor;

import javax.transaction.Transactional;

@AllArgsConstructor
public class DemoService {
    private final EventPublisher eventPublisher;

    @Transactional
    public void run() throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            eventPublisher.publish(new DemoEvent(i + ""), "system1");
            if (i % 100 == 0)
                Thread.sleep(100);
        }
    }
}
