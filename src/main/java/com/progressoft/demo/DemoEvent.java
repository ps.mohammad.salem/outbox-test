package com.progressoft.demo;

import com.progressoft.outbox.Event;
import lombok.Data;

@Data
public class DemoEvent implements Event {
    private final String data;
}
