package com.progressoft;

import com.progressoft.demo.DemoService;
import com.progressoft.outbox.EventPublisher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class OutboxApplication {
    public static void main(String[] args) {
        SpringApplication.run(OutboxApplication.class, args);
    }

    @Bean
    public DemoService demoService(EventPublisher eventPublisher) {
        return new DemoService(eventPublisher);
    }

    @Bean
    public CommandLineRunner testApp(DemoService demoService) {
        return args -> demoService.run();
    }
}
