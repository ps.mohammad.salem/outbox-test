package com.progressoft.outbox;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.progressoft.outbox.EventState.*;

@Slf4j
@AllArgsConstructor
public class EventRelay {
    private final int lockStaleTimeoutInSeconds;
    private final EventSender eventSender;
    private final EventDao eventDao;
    private final EventParser eventParser;
    private final LocalDateTimeSupplier localDateTimeSupplier;

    public void run() {
        String lockToken = UUID.randomUUID().toString();
        LocalDateTime now = localDateTimeSupplier.now();
        LocalDateTime staleLockTimestamp = localDateTimeSupplier.now().minusSeconds(lockStaleTimeoutInSeconds);
        eventDao.lock(lockToken, now, staleLockTimestamp);
        eventDao.getAllByLockTokenAndLockDateTime(lockToken, now).forEach(this::send);
    }

    private void send(EventEntity entity) {
        try {
            eventSender.send(eventParser.parse(entity.getContent(), entity.getEventClass()), entity.getDestination());
            update(entity, Processed, null);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            update(entity, Failed, exceptionToString(ex));
        }
    }

    private void update(EventEntity eventEntity, EventState state, String additionalInfo) {
        eventEntity.setState(state.name());
        eventEntity.setAdditionalInfo(additionalInfo);
        eventEntity.setUpdateDateTime(localDateTimeSupplier.now());
        eventDao.update(eventEntity);
    }

    private String exceptionToString(Exception ex) {
        StringWriter sw = new StringWriter();
        sw.append(ex.getMessage());
        sw.append(System.lineSeparator());
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
