package com.progressoft.outbox;

public interface EventParser {
    Event parse(String content, Class<? extends Event> eventClass);
}
