package com.progressoft.outbox;

public interface EventSerializer {
    String serialize(Event event);
}
