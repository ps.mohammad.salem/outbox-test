package com.progressoft.outbox;

public interface EventPublisher {
    void publish(Event event, String destination);
}
