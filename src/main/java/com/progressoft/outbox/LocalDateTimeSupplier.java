package com.progressoft.outbox;

import java.time.LocalDateTime;

public interface LocalDateTimeSupplier {
    LocalDateTime now();
}
