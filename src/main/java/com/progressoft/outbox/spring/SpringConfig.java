package com.progressoft.outbox.spring;

import com.progressoft.outbox.*;
import com.progressoft.outbox.impl.ConsoleEventSender;
import com.progressoft.outbox.impl.DatabaseEventPublisher;
import com.progressoft.outbox.impl.GsonEventTransformer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class SpringConfig {
    @Bean
    public GsonEventTransformer gsonEventTransformer() {
        return new GsonEventTransformer();
    }

    @Bean
    LocalDateTimeSupplier dateTimeSupplier() {
        return LocalDateTime::now;
    }

    @Bean
    public DatabaseEventPublisher databaseEventPublisher(
            EventDao eventDao,
            EventSerializer eventSerializer,
            LocalDateTimeSupplier localDateTimeSupplier) {
        return new DatabaseEventPublisher(eventDao, eventSerializer, localDateTimeSupplier);
    }

    @Bean
    public DestinationBindings destinationBindings() {
        return DestinationBindings.same();
    }

    @Bean
    public ConsoleEventSender consoleEventSender(DestinationBindings destinationBindings) {
        return new ConsoleEventSender(destinationBindings);
    }

    @Bean
    public SpringEventRelay springEventRelay(
            @Value("${outbox.lock.stale.timeout.in.seconds:20}") int lockStaleTimeoutInSeconds,
            EventSender eventSender,
            EventDao eventDao,
            EventParser eventParser,
            LocalDateTimeSupplier localDateTimeSupplier) {
        return new SpringEventRelay(
                new EventRelay(
                        lockStaleTimeoutInSeconds,
                        eventSender,
                        eventDao,
                        eventParser,
                        localDateTimeSupplier));
    }
}
