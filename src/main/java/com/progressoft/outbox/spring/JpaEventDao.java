package com.progressoft.outbox.spring;

import com.progressoft.outbox.EventDao;
import com.progressoft.outbox.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static javax.transaction.Transactional.TxType.REQUIRES_NEW;

@Repository
public interface JpaEventDao extends EventDao, JpaRepository<EventEntity, String> {
    List<EventEntity> getAllByLockTokenAndLockDateTime(String lockToken, LocalDateTime localDateTime);

    @Transactional(REQUIRES_NEW)
    @Modifying @Query(
            "update EventEntity set lockToken=:lockToken, lockDateTime=:now " +
            "WHERE state = 'New' and (lockToken is null or lockDateTime <= :staleLockTimestamp)")
    void lock(String lockToken, LocalDateTime now, LocalDateTime staleLockTimestamp);

    @Override
    default void insert(EventEntity eventEntity) {
        save(eventEntity);
    }

    @Transactional(REQUIRES_NEW)
    @Override
    default void update(EventEntity eventEntity) {
        save(eventEntity);
    }
}
