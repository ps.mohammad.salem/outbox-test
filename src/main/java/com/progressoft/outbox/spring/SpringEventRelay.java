package com.progressoft.outbox.spring;

import com.progressoft.outbox.EventRelay;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;

@AllArgsConstructor
public class SpringEventRelay {
    private final EventRelay eventRelay;

    @Scheduled(fixedRateString = "${outbox.event.relay.in.milliseconds:100}")
    public void run() {
        eventRelay.run();
    }
}
