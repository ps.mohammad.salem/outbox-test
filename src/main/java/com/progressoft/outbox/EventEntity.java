package com.progressoft.outbox;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventEntity {
    @Id
    private String id;
    private String content;
    private String type;
    private String destination;
    private LocalDateTime creationDateTime;
    private LocalDateTime updateDateTime;
    private String state;
    private String additionalInfo;
    private String lockToken;
    private LocalDateTime lockDateTime;

    public Class<? extends Event> getEventClass() {
        try {
            return (Class<? extends Event>) Class.forName(type);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
