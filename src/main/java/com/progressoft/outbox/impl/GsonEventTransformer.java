package com.progressoft.outbox.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.outbox.Event;
import com.progressoft.outbox.EventParser;
import com.progressoft.outbox.EventSerializer;

public class GsonEventTransformer implements EventSerializer, EventParser {
    private final Gson gson = new GsonBuilder().create();

    @Override
    public String serialize(Event event) {
        return gson.toJson(event);
    }

    @Override
    public Event parse(String content, Class<? extends Event> eventClass) {
        return gson.fromJson(content, eventClass);
    }
}
