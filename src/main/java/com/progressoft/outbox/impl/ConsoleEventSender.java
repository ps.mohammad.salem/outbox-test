package com.progressoft.outbox.impl;

import com.progressoft.outbox.DestinationBindings;
import com.progressoft.outbox.Event;
import com.progressoft.outbox.EventSender;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConsoleEventSender implements EventSender {
    private final DestinationBindings destinationBindings;

    @Override
    public void send(Event event, String destination) {
        System.out.println("sending " + event + " to " + destinationBindings.resolve(destination));
    }
}
