package com.progressoft.outbox.impl;

import com.progressoft.outbox.*;
import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class DatabaseEventPublisher implements EventPublisher {
    private final EventDao eventDao;
    private final EventSerializer eventSerializer;
    private final LocalDateTimeSupplier localDateTimeSupplier;

    @Override
    public void publish(Event event, String destination) {
        eventDao.insert(toEntity(event, destination));
    }

    private EventEntity toEntity(Event event, String destination) {
        return EventEntity.builder()
                .id(UUID.randomUUID().toString())
                .content(eventSerializer.serialize(event))
                .type(event.getClass().getName())
                .destination(destination)
                .creationDateTime(localDateTimeSupplier.now())
                .updateDateTime(localDateTimeSupplier.now())
                .state(EventState.New.name())
                .build();
    }
}
