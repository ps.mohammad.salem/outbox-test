package com.progressoft.outbox;

import java.time.LocalDateTime;
import java.util.List;

public interface EventDao {
    List<EventEntity> getAllByLockTokenAndLockDateTime(String lockToken, LocalDateTime localDateTime);

    void lock(String lockToken, LocalDateTime now, LocalDateTime staleLockTimestamp);

    void insert(EventEntity eventEntity);

    void update(EventEntity eventEntity);
}
