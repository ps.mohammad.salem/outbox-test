package com.progressoft.outbox;

public enum EventState {
    New, Processed, Failed;
}
