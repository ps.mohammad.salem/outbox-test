package com.progressoft.outbox;

public interface EventSender {
    void send(Event event, String destination);
}
